OBJS = main.o uart.o
TARGET = payload.bin
.PHONY: clean
all: payload.bin

CFLAGS := -target aarch64 -ffreestanding -mgeneral-regs-only -mstrict-align
LDFLAGS := -Bstatic -Tpayload.lds -nostdlib

CC = clang $(CFLAGS)
LD = ld.lld $(LDFLAGS)
OBJCOPY = llvm-objcopy

.c.o:
	@echo " [CC]   $<"
	@$(CC) $< -c -o $@

.S.o:
	@echo " [CC]   $<"
	@$(CC) $< -c -o $@

payload.elf: $(OBJS) payload.lds
	@echo " [LD]   payload.elf"
	@$(LD) $(OBJS) -o payload.elf

payload.bin: payload.elf
	@echo " [CP]   payload.bin"
	@$(OBJCOPY) -O binary payload.elf payload.bin

clean:
	rm -f $(OBJS)
	rm -f payload.elf
	rm -f payload.bin
