# RK356x Bare Metal Hello World
This is a relatively simple test of running bare metal code on the RK3566/RK3588, all it can do is print strings to the debug UART.

## Compiling/Running
Dependencies are llvm, clang and lld. Nothing special should be necessary to build the binary.
The easiest way to run this is on a board in maskrom mode with rkdeveloptool, just use the boot_merger tool to create a payload from a ddr init blob and the built payload.bin, then boot that on the device.
