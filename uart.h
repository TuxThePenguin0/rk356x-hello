#ifndef UART_H
#define UART_H

#include "types.h"

static u8* const UART_DEBUG = (u8*)0xfe660000;

void uartWrite(u8* const uart, char* output, int maxLength);

#endif
