#include "uart.h"

void uartWrite(u8* const uart, char* output, int maxLength) {
	for (int i = 0; output[i] != '\0' && i < maxLength; i++) {
		*uart = output[i];
	}
}
